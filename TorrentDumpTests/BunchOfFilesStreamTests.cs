﻿// Copyright (c) cnaos 2017-2019
// All rights reserved.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TorrentDump.model;

namespace TorrentDump.Tests
{
    [TestClass]
    public class BunchOfFilesStreamTests
    {
        /// <summary>
        /// </summary>
        private List<TorrentFileInfo> scanDir(string path)
        {
            var dir = new DirectoryInfo(path);

            var testFileList = dir.GetFiles("*.txt");
            Array.Sort(testFileList, (a, b) => a.Name.CompareTo(b.Name));

            var fileList = new List<TorrentFileInfo>();
            return testFileList
                .Select((fileInfo, index) => new TorrentFileInfo(index, fileInfo.FullName, fileInfo.Length)).ToList();
        }


        [TestMethod]
        public void BunchOfFilesStreamTest()
        {
            var testFileList = scanDir("TestData/DataSet1");
            Assert.IsNotNull(testFileList);
            Assert.AreEqual(5, testFileList.Count);


            var stream = new BunchOfFilesStream(testFileList);
            Assert.AreEqual(30, stream.Length);
        }


        [TestMethod]
        [TestCategory("Seekのテスト")]
        public void SeekTest_offset0()
        {
            const long targetOffset = 0;
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            var result = stream.Seek(targetOffset, SeekOrigin.Begin);
            Assert.AreEqual(0, result);

            var selectedFileInfo = stream.SelectedFileInfo;
            Assert.AreEqual("TextFile1.txt", Path.GetFileName(selectedFileInfo.Path));
            Assert.AreEqual(0, stream.SelectedFileOffset);
        }

        [TestMethod]
        [TestCategory("Seekのテスト")]
        public void SeekTest_offset1()
        {
            const long targetOffset = 1;
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            var result = stream.Seek(targetOffset, SeekOrigin.Begin);
            Assert.AreEqual(1, result);

            var selectedFileInfo = stream.SelectedFileInfo;
            Assert.AreEqual("TextFile1.txt", Path.GetFileName(selectedFileInfo.Path));
            Assert.AreEqual(1, stream.SelectedFileOffset);
        }

        [TestMethod]
        [TestCategory("Seekのテスト")]
        public void SeekTest_offset3()
        {
            const long targetOffset = 3;
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            var result = stream.Seek(targetOffset, SeekOrigin.Begin);
            Assert.AreEqual(3, result);

            var selectedFileInfo = stream.SelectedFileInfo;
            Assert.AreEqual("TextFile1.txt", Path.GetFileName(selectedFileInfo.Path));
            Assert.AreEqual(3, stream.SelectedFileOffset);
        }

        [TestMethod]
        [TestCategory("Seekのテスト")]
        public void SeekTest_offset6()
        {
            const long targetOffset = 6;
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            var result = stream.Seek(targetOffset, SeekOrigin.Begin);
            Assert.AreEqual(6, result);

            var selectedFileInfo = stream.SelectedFileInfo;
            Assert.AreEqual("TextFile2.txt", Path.GetFileName(selectedFileInfo.Path));
            Assert.AreEqual(0, stream.SelectedFileOffset);
        }

        [TestMethod]
        [TestCategory("Seekのテスト")]
        public void SeekTest_offset7()
        {
            const long targetOffset = 7;
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            var result = stream.Seek(targetOffset, SeekOrigin.Begin);
            Assert.AreEqual(7, result);

            var selectedFileInfo = stream.SelectedFileInfo;
            Assert.AreEqual("TextFile2.txt", Path.GetFileName(selectedFileInfo.Path));
            Assert.AreEqual(1, stream.SelectedFileOffset);
        }

        [TestMethod]
        [TestCategory("Seekのテスト")]
        public void SeekTest_offsetMax()
        {
            const long targetOffset = 29; // sizeが30なのでoffset29がMAX
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            var result = stream.Seek(targetOffset, SeekOrigin.Begin);
            Assert.AreEqual(29, result);

            var selectedFileInfo = stream.SelectedFileInfo;
            Assert.AreEqual("TextFile5.txt", Path.GetFileName(selectedFileInfo.Path));
            Assert.AreEqual(5, stream.SelectedFileOffset);
        }

        [TestMethod]
        [TestCategory("Seekのテスト")]
        public void SeekTest_offsetMaxPlus1_error()
        {
            const long targetOffset = 6 * 5; // ファイル合計のsizeが30なのoffsetだとMaxよりも1多い
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            try
            {
                var result = stream.Seek(targetOffset, SeekOrigin.Begin);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Assert.AreEqual("offset", e.ParamName);
                Assert.AreEqual(30L, e.ActualValue);
                return;
            }

            Assert.Fail("expected Exception not occured");
        }


        [TestMethod]
        [TestCategory("Readのテスト")]
        public void ReadTest_offset0_read1()
        {
            var buffer = new byte[10];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            long result = stream.Read(buffer, 0, 1);
            Assert.AreEqual(1, result);
            CollectionAssert.AreEqual(new byte[] {(byte) 'A', 0, 0, 0, 0, 0, 0, 0, 0, 0}, buffer);
        }

        [TestMethod]
        [TestCategory("Readのテスト")]
        public void ReadTest_offset0_read2()
        {
            var buffer = new byte[10];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            long result = stream.Read(buffer, 0, 2);
            Assert.AreEqual(2, result);
            CollectionAssert.AreEqual(new byte[] {(byte) 'A', (byte) 'A', 0, 0, 0, 0, 0, 0, 0, 0}, buffer);
        }

        [TestMethod]
        [TestCategory("Readのテスト")]
        public void ReadTest_offset0_read9()
        {
            var buffer = new byte[10];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            long result = stream.Read(buffer, 0, 9);
            Assert.AreEqual(9, result);
            var expectedBytes = Encoding.ASCII.GetBytes("AAAA\r\nBBB\0");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }

        [TestMethod]
        [TestCategory("Readのテスト")]
        public void ReadTest_offset0_read10()
        {
            var buffer = new byte[10];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            long result = stream.Read(buffer, 0, 10);
            Assert.AreEqual(10, result);
            var expectedBytes = Encoding.ASCII.GetBytes("AAAA\r\nBBBB");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }

        [TestMethod]
        [TestCategory("Readのテスト")]
        public void ReadTest_offset0_read11_error()
        {
            var buffer = new byte[10];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            try
            {
                long result = stream.Read(buffer, 0, 11);
            }
            catch (ArgumentException e)
            {
                return;
            }

            Assert.Fail("expected Exception not occured");
        }

        [TestMethod]
        [TestCategory("Readのテスト")]
        public void ReadTest_offset0_read30()
        {
            var buffer = new byte[30];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            long result = stream.Read(buffer, 0, 30);
            Assert.AreEqual(30, result);
            var expectedBytes = Encoding.ASCII.GetBytes("AAAA\r\nBBBB\r\nCCCC\r\nDDDD\r\nEEEE\r\n");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }

        [TestMethod]
        [TestCategory("Readのテスト")]
        public void ReadTest_offset0_read31()
        {
            var buffer = new byte[31];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            long result = stream.Read(buffer, 0, 31);
            Assert.AreEqual(30, result);
            var expectedBytes = Encoding.ASCII.GetBytes("AAAA\r\nBBBB\r\nCCCC\r\nDDDD\r\nEEEE\r\n\0");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }


        [TestMethod]
        [TestCategory("Readのテスト")]
        public void ReadTest_offset1_read9()
        {
            var buffer = new byte[10];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            long result = stream.Read(buffer, 1, 9);
            Assert.AreEqual(9, result);
            var expectedBytes = Encoding.ASCII.GetBytes("\0AAAA\r\nBBB");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }

        [TestMethod]
        [TestCategory("Readのテスト")]
        public void ReadTest_offset1_read10_error()
        {
            var buffer = new byte[10];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            try
            {
                long result = stream.Read(buffer, 1, 10);
            }
            catch (ArgumentException e)
            {
                return;
            }

            Assert.Fail("expected Exception not occured");
        }


        [TestMethod]
        [TestCategory("Readのテスト")]
        public void ReadTest_offset9_read1()
        {
            var buffer = new byte[10];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            long result = stream.Read(buffer, 9, 1);
            Assert.AreEqual(1, result);
            var expectedBytes = Encoding.ASCII.GetBytes("\0\0\0\0\0\0\0\0\0A");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }

        [TestMethod]
        [TestCategory("繰り返しReadのテスト")]
        public void ReadRepadTest_read3()
        {
            var buffer = new byte[4];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);

            {
                long result1 = stream.Read(buffer, 0, 4);
                Assert.AreEqual(4, result1);
                var expectedBytes1 = Encoding.ASCII.GetBytes("AAAA");
                CollectionAssert.AreEqual(expectedBytes1, buffer);
            }
            {
                long result2 = stream.Read(buffer, 0, 4);
                Assert.AreEqual(4, result2);
                var expectedBytes2 = Encoding.ASCII.GetBytes("\r\nBB");
                CollectionAssert.AreEqual(expectedBytes2, buffer);
            }
            {
                long result3 = stream.Read(buffer, 0, 4);
                Assert.AreEqual(4, result3);
                var expectedBytes3 = Encoding.ASCII.GetBytes("BB\r\n");
                CollectionAssert.AreEqual(expectedBytes3, buffer);
            }
        }


        [TestMethod]
        [TestCategory("SeekとReadのテスト")]
        public void SeekReadTest_seek1_read10()
        {
            var buffer = new byte[10];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);
            var resultSeeK = stream.Seek(1, SeekOrigin.Begin);
            Assert.AreEqual(1, resultSeeK);

            long resultRead = stream.Read(buffer, 0, 10);
            Assert.AreEqual(10, resultRead);
            var expectedBytes = Encoding.ASCII.GetBytes("AAA\r\nBBBB\r");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }

        [TestMethod]
        [TestCategory("SeekとReadのテスト")]
        public void SeekReadTest_seek20_read10()
        {
            var buffer = new byte[10];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);
            var resultSeeK = stream.Seek(20, SeekOrigin.Begin);
            Assert.AreEqual(20, resultSeeK);

            long resultRead = stream.Read(buffer, 0, 10);
            Assert.AreEqual(10, resultRead);
            var expectedBytes = Encoding.ASCII.GetBytes("DD\r\nEEEE\r\n");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }


        [TestMethod]
        [TestCategory("SeekとReadのテスト")]
        public void SeekReadTest()
        {
            var buffer = new byte[10];
            var testFileList = scanDir("TestData/DataSet1");

            var stream = new BunchOfFilesStream(testFileList);
            {
                var resultSeeK1 = stream.Seek(1, SeekOrigin.Begin);
                Assert.AreEqual(1, resultSeeK1);
            }
            {
                long resultRead1 = stream.Read(buffer, 0, 10);
                Assert.AreEqual(10, resultRead1);
                var expectedBytes = Encoding.ASCII.GetBytes("AAA\r\nBBBB\r");
                CollectionAssert.AreEqual(expectedBytes, buffer);
            }
        }

        [TestMethod]
        [TestCategory("不完全なファイルが含まれる場合のReadのテスト")]
        public void ReadTest_contain_notExistFile1()
        {
            var buffer = new byte[10];
            var testFileList = scanDir("TestData/DataSet1");
            var dummyFileInfo = new TorrentFileInfo(0, "DUMMY", 4);
            testFileList.Insert(0, dummyFileInfo);

            var stream = new BunchOfFilesStream(testFileList);

            long resultRead = stream.Read(buffer, 0, 10);
            Assert.AreEqual(10, resultRead);
            var expectedBytes = Encoding.ASCII.GetBytes("\0\0\0\0AAAA\r\n");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }

        [TestMethod]
        [TestCategory("不完全なファイルが含まれる場合のReadのテスト")]
        public void ReadTest_contain_notExistFile2()
        {
            var buffer = new byte[14];
            var testFileList = scanDir("TestData/DataSet1");
            var dummyFileInfo = new TorrentFileInfo(0, "DUMMY", 4);
            testFileList.Insert(1, dummyFileInfo);

            var stream = new BunchOfFilesStream(testFileList);

            long resultRead = stream.Read(buffer, 0, 14);
            Assert.AreEqual(14, resultRead);
            var expectedBytes = Encoding.ASCII.GetBytes("AAAA\r\n\0\0\0\0BBBB");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }

        [TestMethod]
        [TestCategory("不完全なファイルが含まれる場合のReadのテスト")]
        public void ReadTest_contain_notExistFile3()
        {
            // バッファ・サイズがTorrentInfoのサイズよりも小さい
            var buffer = new byte[14];
            var testFileList = scanDir("TestData/DataSet1");
            var dummyFileInfo = new TorrentFileInfo(0, "DUMMY", 20);
            testFileList.Insert(1, dummyFileInfo);

            var stream = new BunchOfFilesStream(testFileList);

            {
                long resultRead1 = stream.Read(buffer, 0, 14);
                Assert.AreEqual(14, resultRead1);
                var expectedBytes1 = Encoding.ASCII.GetBytes("AAAA\r\n\0\0\0\0\0\0\0\0");
                CollectionAssert.AreEqual(expectedBytes1, buffer);
            }

            {
                long resultRead = stream.Read(buffer, 0, 14);
                Assert.AreEqual(14, resultRead);
                var expectedBytes = Encoding.ASCII.GetBytes("\0\0\0\0\0\0\0\0\0\0\0\0BB");
                CollectionAssert.AreEqual(expectedBytes, buffer);
            }
        }

        [TestMethod]
        [TestCategory("不完全なファイルが含まれる場合のReadのテスト")]
        public void ReadTest_contain_shortFile1()
        {
            var buffer = new byte[15];
            var testFileList = scanDir("TestData/DataSet1");
            var testFile1 = new TorrentFileInfo(0, testFileList[0].Path, testFileList[0].Size + 1);
            testFileList[0] = testFile1;

            var stream = new BunchOfFilesStream(testFileList);

            long resultRead = stream.Read(buffer, 0, 15);
            Assert.AreEqual(15, resultRead);
            var expectedBytes = Encoding.ASCII.GetBytes("AAAA\r\n\0BBBB\r\nCC");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }

        [TestMethod]
        [TestCategory("不完全なファイルが含まれる場合のReadのテスト")]
        public void ReadTest_contain_shortFile2()
        {
            var buffer = new byte[15];
            var testFileList = scanDir("TestData/DataSet1");
            var testFile1 = new TorrentFileInfo(0, testFileList[1].Path, testFileList[1].Size + 1);
            testFileList[1] = testFile1;

            var stream = new BunchOfFilesStream(testFileList);

            long resultRead = stream.Read(buffer, 0, 15);
            Assert.AreEqual(15, resultRead);
            var expectedBytes = Encoding.ASCII.GetBytes("AAAA\r\nBBBB\r\n\0CC");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }

        [TestMethod]
        [TestCategory("不完全なファイルが含まれる場合のReadのテスト")]
        public void ReadTest_contain_shortFile3()
        {
            var buffer = new byte[5];
            var testFileList = scanDir("TestData/DataSet1");
            var testFile1 = new TorrentFileInfo(0, testFileList[1].Path, testFileList[1].Size + 1);
            testFileList[1] = testFile1;

            var stream = new BunchOfFilesStream(testFileList);
            {
                long resultRead1 = stream.Read(buffer, 0, 5);
                Assert.AreEqual(5, resultRead1);
                var expectedBytes1 = Encoding.ASCII.GetBytes("AAAA\r");
                CollectionAssert.AreEqual(expectedBytes1, buffer);
            }
            {
                long resultRead2 = stream.Read(buffer, 0, 5);
                Assert.AreEqual(5, resultRead2);
                var expectedBytes2 = Encoding.ASCII.GetBytes("\nBBBB");
                CollectionAssert.AreEqual(expectedBytes2, buffer);
            }
            {
                long resultRead3 = stream.Read(buffer, 0, 5);
                Assert.AreEqual(5, resultRead3);
                var expectedBytes3 = Encoding.ASCII.GetBytes("\r\n\0CC");
                CollectionAssert.AreEqual(expectedBytes3, buffer);
            }
        }

        [TestMethod]
        [TestCategory("不完全なファイルが含まれる場合のReadのテスト")]
        public void ReadTest_contain_longFile1()
        {
            var buffer = new byte[15];
            var testFileList = scanDir("TestData/DataSet1");
            var testFile1 = new TorrentFileInfo(0, testFileList[0].Path, testFileList[0].Size - 3);
            testFileList[0] = testFile1;

            var stream = new BunchOfFilesStream(testFileList);

            long resultRead = stream.Read(buffer, 0, 15);
            Assert.AreEqual(15, resultRead);
            var expectedBytes = Encoding.ASCII.GetBytes("AAABBBB\r\nCCCC\r\n");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }

        [TestMethod]
        [TestCategory("不完全なファイルが含まれる場合のReadのテスト")]
        public void ReadTest_contain_longFile2()
        {
            var buffer = new byte[15];
            var testFileList = scanDir("TestData/DataSet1");
            var testFile1 = new TorrentFileInfo(0, testFileList[1].Path, testFileList[1].Size - 3);
            testFileList[1] = testFile1;

            var stream = new BunchOfFilesStream(testFileList);

            long resultRead = stream.Read(buffer, 0, 15);
            Assert.AreEqual(15, resultRead);
            var expectedBytes = Encoding.ASCII.GetBytes("AAAA\r\nBBBCCCC\r\n");
            CollectionAssert.AreEqual(expectedBytes, buffer);
        }

        [TestMethod]
        [TestCategory("不完全なファイルが含まれる場合のReadのテスト")]
        public void ReadTest_contain_longFile3()
        {
            var buffer = new byte[5];
            var testFileList = scanDir("TestData/DataSet1");
            var testFile1 = new TorrentFileInfo(0, testFileList[1].Path, testFileList[1].Size - 3);
            testFileList[1] = testFile1;

            var stream = new BunchOfFilesStream(testFileList);

            {
                long resultRead1 = stream.Read(buffer, 0, 5);
                Assert.AreEqual(5, resultRead1);
                var expectedBytes1 = Encoding.ASCII.GetBytes("AAAA\r");
                CollectionAssert.AreEqual(expectedBytes1, buffer);
            }
            {
                long resultRead2 = stream.Read(buffer, 0, 5);
                Assert.AreEqual(5, resultRead2);
                var expectedBytes2 = Encoding.ASCII.GetBytes("\nBBBC");
                CollectionAssert.AreEqual(expectedBytes2, buffer);
            }
            {
                long resultRead3 = stream.Read(buffer, 0, 5);
                Assert.AreEqual(5, resultRead3);
                var expectedBytes3 = Encoding.ASCII.GetBytes("CCC\r\n");
                CollectionAssert.AreEqual(expectedBytes3, buffer);
            }
        }
    }
}