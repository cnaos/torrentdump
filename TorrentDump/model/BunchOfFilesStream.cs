﻿// Copyright (c) cnaos 2016-2019
// All rights reserved.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace TorrentDump.model
{
    public class BunchOfFilesStream : Stream
    {
        private bool _disposed = false;
        private long _position;

        public BunchOfFilesStream(List<TorrentFileInfo> argFileList)
        {
            FileList = new List<TorrentFileInfo>(argFileList);
            _position = 0;
            Length = argFileList.Select(file => file.Size).Sum();
        }

        public BunchOfFilesStream(string directory, List<TorrentFileInfo> torrentFileInfoList) : this(
            torrentFileInfoList)
        {
            Directory = directory;
        }

        private List<TorrentFileInfo> FileList { get; }
        private string Directory { get; } = "";

        public TorrentFileInfo SelectedFileInfo { get; private set; }
        public long SelectedFileOffset { get; private set; }


        public override bool CanRead => true;
        public override bool CanSeek { get; }

        public override bool CanWrite => false;

        public override long Length { get; }

        public override long Position
        {
            get => _position;
            set => throw new NotSupportedException();
        }

        public override void Flush()
        {
            throw new NotImplementedException();
        }

        private Tuple<TorrentFileInfo, long> LocalSeek(long offset)
        {
            var maxOffset = Math.Max(0, Length - 1);
            if (maxOffset < offset)
            {
                throw new ArgumentOutOfRangeException("offset", offset, $"max offset={maxOffset}");
            }

            long seekPos = 0;

            var selectedFile = FileList.First(delegate(TorrentFileInfo info)
            {
                if (seekPos + info.Size > offset)
                {
                    return true;
                }

                seekPos += info.Size;
                return false;
            });

            var selectedFileOffset = offset - seekPos;
            Debug.Assert(selectedFileOffset >= 0);

            var ret = Tuple.Create(selectedFile, selectedFileOffset);
            return ret;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            if (origin != SeekOrigin.Begin)
            {
                throw new NotImplementedException();
            }

            var seekResult = LocalSeek(offset);
            SelectedFileInfo = seekResult.Item1;
            SelectedFileOffset = seekResult.Item2;
            _position = offset;

            return offset;
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }


        public override int Read(byte[] buffer, int offset, int count)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }

            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException("offset", offset, "offset is negative value");
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException("count", count, "count is negative value");
            }

            if (buffer.Length < offset + count)
            {
                throw new ArgumentException("offset と count の合計値が、バッファー長より大きい値です。");
            }

            var readCount = count;
            var totalReadBytes = 0;
            var innerOffset = offset;
            do
            {
                // 現在位置からアクセス対象のファイルを選定
                Seek(_position, SeekOrigin.Begin);

                var readBytes = ReadToBuffer(SelectedFileInfo, SelectedFileOffset, buffer, innerOffset, readCount);

                totalReadBytes += readBytes;
                _position += readBytes;
                innerOffset += readBytes;
                readCount -= readBytes;

                if (_position >= Length)
                {
                    break;
                }

                // バッファに指定countだけ読み込めたかチェック
            } while (totalReadBytes < count);

            return totalReadBytes;
        }

        /// <summary>
        ///     ファイルからバッファにデータを読み込む。
        ///     ファイルが存在しない場合はゼロフィルする
        /// </summary>
        /// <param name="selectedFileInfo"></param>
        /// <param name="fileSeekOffset"></param>
        /// <param name="buffer">ファイルからデータを読み込むバッファ</param>
        /// <param name="offset">バッファに読み込んだデータを書き込む最初の位置</param>
        /// <param name="readCount">読み込みバイト数</param>
        /// <returns></returns>
        private int ReadToBuffer(TorrentFileInfo selectedFileInfo, long fileSeekOffset, byte[] buffer, int offset,
            int readCount)
        {
            // TODO offsetの指定をlongにする？でないと4Gバイト以上のファイルが扱えない
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }

            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException("offset", offset, "offset is negative value");
            }

            if (readCount < 0)
            {
                throw new ArgumentOutOfRangeException("readCount", readCount, "readCount is negative value");
            }

            if (buffer.Length < offset + readCount)
            {
                throw new ArgumentException("offset と readCount の合計値が、バッファー長より大きい値です。");
            }

            var readBytes = 0;
            var realFilePath = Path.Combine(Directory, selectedFileInfo.Path);
            if (File.Exists(realFilePath))
            {
                using (var fs = new FileStream(realFilePath, FileMode.Open, FileAccess.Read))
                {
                    // 実際のファイルサイズをチェックしてreadCountを制限する
                    checked
                    {
                        var fileSizeDiff = fs.Length - selectedFileInfo.Size;
                        if (fileSizeDiff > 0)
                        {
                            var maxReadCount = selectedFileInfo.Size;
                            maxReadCount -= fileSeekOffset;
                            readCount = (int) Math.Min(maxReadCount, readCount);
                        }
                    }

                    // アクセス対象のファイルをシーク
                    fs.Seek(fileSeekOffset, SeekOrigin.Begin);
                    if (fs.Length == fileSeekOffset)
                    {
                        checked
                        {
                            var skipBytes = (int) (selectedFileInfo.Size - fileSeekOffset);
                            Array.Clear(buffer, offset, skipBytes);
                            readBytes = skipBytes;
                        }
                    }
                    else
                    {
                        // バッファに読み込み
                        readBytes = fs.Read(buffer, offset, readCount);
                    }
                }
            }
            else
            {
                // ファイルが見つからなかったらその部分をゼロフィル
                checked
                {
                    var bufferRemain = buffer.Length - offset;
                    var skipBytes = Math.Min((int) (selectedFileInfo.Size - fileSeekOffset), bufferRemain);


                    Array.Clear(buffer, offset, skipBytes);
                    readBytes = skipBytes;
                }
            }

            return readBytes;
        }


        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }
    }
}