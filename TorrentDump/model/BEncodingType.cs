﻿// Copyright (c) cnaos 2017-2019
// All rights reserved.

using System.Diagnostics;
using BencodeNET.Objects;

namespace TorrentDump.model
{
    public enum BEncodingType
    {
        BString = 1,
        BNumber = 2,
        BList = 3,
        BDictionary = 4,
        UNDEFINED = -1
    }

    public static class BEncodingTypeExtensions
    {
        public static BEncodingType ParseType(IBObject iboject)
        {
            object objValue = iboject;


            if (objValue is BString)
            {
                return BEncodingType.BString;
            }

            if (objValue is BNumber)
            {
                return BEncodingType.BNumber;
            }

            if (objValue is BList)
            {
                return BEncodingType.BList;
            }

            if (objValue is BDictionary)
            {
                return BEncodingType.BDictionary;
            }

            Debug.Assert(false, "UNDEFINED BEncoding Type");
            return BEncodingType.UNDEFINED;
        }
    }
}