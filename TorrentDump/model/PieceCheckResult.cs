﻿// Copyright (c) cnaos 2017-2019
// All rights reserved.

namespace TorrentDump.model
{
    public class PieceCheckResult
    {
        public PieceCheckResult(int index, string hash)
        {
            Index = index;
            PieceHash = hash;
        }

        public int Index { get; }
        public string PieceHash { get; }
        public string ActualFileHash { get; set; }
        public string FilePath { get; set; }

        public bool IsSameHash => PieceHash == ActualFileHash;
    }
}