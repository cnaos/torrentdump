﻿// Copyright (c) cnaos 2017-2019
// All rights reserved.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using BencodeNET.Objects;
using BencodeNET.Parsing;
using BencodeNET.Torrents;

namespace TorrentDump.model
{
    internal class TorrentXmlConverter
    {
        public XDocument convertTorrentStream(Stream inStream)
        {
            // BEncodeのStreamを解析してXMLを構築する
            var doc = new XDocument(new XDeclaration("1.0", "utf-8", null),
                new XElement("torrent"));
            var xmlRootElement = doc.Root;
            var parser = new BencodeParser();
            // TODO ここでtorrentファイルのパースエラーが起きた時の例外処理が必要
            var torrent = parser.Parse<Torrent>(inStream);
            ProcRecursive(xmlRootElement, torrent.ToBDictionary());

            // piecesの処理
            SplitPiecesToText(xmlRootElement);

            return doc;
        }

        private void SplitPiecesToText(XElement piecesNode)
        {
            var pieceSringElement = piecesNode.XPathSelectElement("//entry[@key='pieces']/str");
            var piecesElement = pieceSringElement.Parent;
            var pieceBSgring = pieceSringElement.Annotation<BString>();
            var piecesBytes = pieceBSgring.Value.ToArray();

            Debug.Assert(piecesBytes.Length % 20 == 0);

            var hashList = SplitPieceHash(piecesBytes);
            pieceSringElement.Remove();

            foreach (var hash in hashList)
            {
                var sha1Hash = new XElement("sha1", hash);
                piecesElement.Add(sha1Hash);
            }
        }

        private List<string> SplitPieceHash(byte[] piecesBytes)
        {
            var piecHashList = new List<string>();
            var sha1HashBuffer = new byte[20];
            int read;
            using (var ms = new MemoryStream(piecesBytes))
            {
                while ((read = ms.Read(sha1HashBuffer, 0, sha1HashBuffer.Length)) > 0)
                {
                    var sha1Hash = BitConverter.ToString(sha1HashBuffer).Replace("-", "");
                    ;
                    piecHashList.Add(sha1Hash);
                }
            }

            return piecHashList;
        }

        private void ProcRecursive(XElement parentNode, IBObject iBObject)
        {
            var type = BEncodingTypeExtensions.ParseType(iBObject);
            switch (type)
            {
                case BEncodingType.BString:
                {
                    var strBString = (BString) iBObject;
                    var label = strBString.ToString();

                    // TODO ここで保存するときにstringからバイト列に相互変換できるオブジェクトに変換するといいかも。
                    var strNode = new XElement("str", label);
                    strNode.AddAnnotation(strBString);

                    parentNode.Add(strNode);
                    return;
                }

                case BEncodingType.BNumber:
                {
                    var number = (BNumber) iBObject;
                    var label = number.ToString();
                    var numberNode = new XElement("integer", label);
                    numberNode.AddAnnotation(number);

                    parentNode.Add(numberNode);
                    return;
                }

                case BEncodingType.BList:
                {
                    var xmlList = new XElement("list");
                    var list = (BList) iBObject;
                    foreach (var subValue in list)
                    {
                        ProcRecursive(xmlList, subValue);
                    }

                    parentNode.Add(xmlList);
                    return;
                }
                case BEncodingType.BDictionary:
                {
                    var xmlDict = new XElement("dict");
                    var bdictionary = (BDictionary) iBObject;
                    foreach (var entry in bdictionary)
                    {
                        var key = entry.Key.ToString();
                        var value = entry.Value;

                        var xmlDictEntry = new XElement("entry");
                        xmlDictEntry.SetAttributeValue("key", key);
                        ProcRecursive(xmlDictEntry, value);

                        xmlDict.Add(xmlDictEntry);
                    }

                    parentNode.Add(xmlDict);
                    return;
                }
            }
        }
    }
}