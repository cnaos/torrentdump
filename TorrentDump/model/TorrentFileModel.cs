﻿// Copyright (c) cnaos 2016-2019
// All rights reserved.

using System.Collections.Generic;
using System.Xml.Linq;

namespace TorrentDump.model
{
    public class TorrentFileModel
    {
        public XDocument XDocument { get; internal set; }
        public List<TorrentFileInfo> TorrentFileInfoList { get; internal set; }
        public List<string> PieceHashList { get; internal set; } = new List<string>();
    }
}