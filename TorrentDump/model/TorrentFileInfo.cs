﻿// Copyright (c) cnaos 2017-2019
// All rights reserved.

namespace TorrentDump.model
{
    public class TorrentFileInfo
    {
        public TorrentFileInfo(int index, string path, long size)
        {
            Index = index;
            Path = path;
            Size = size;
        }

        public int Index { get; }
        public string Path { get; }
        public long Size { get; }
        public string FileHash { get; set; }
    }
}