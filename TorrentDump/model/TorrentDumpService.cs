﻿// Copyright (c) cnaos 2017-2019
// All rights reserved.

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.XPath;

namespace TorrentDump.model
{
    public class TorrentDumpService
    {
        public TorrentFileModel ParseTorrentFile(string torrentFilePath)
        {
            var converter = new TorrentXmlConverter();

            var model = new TorrentFileModel();

            using (var fsSource = new FileStream(torrentFilePath, FileMode.Open, FileAccess.Read))
            {
                var doc = converter.convertTorrentStream(fsSource);
                var xmlDocument = new XmlDocument();
                using (var xmlReader = doc.CreateReader())
                {
                    xmlDocument.Load(xmlReader);
                }

                model.XDocument = doc;
            }

            // ファイルリストの収集
            var fileList = new List<TorrentFileInfo>();
            var root = model.XDocument;
            var fileNodeList =
                root.XPathSelectElements("/torrent/dict/entry[@key='info']/dict/entry[@key='files']/list/*");
            if (!fileNodeList.Any())
            {
                // single file

                var nameNode = root.XPathSelectElement("/torrent/dict/entry[@key='info']/dict/entry[@key='name']");
                var filepath = nameNode?.Value ?? "not found";

                var lengthNode = root.XPathSelectElement("/torrent/dict/entry[@key='info']/dict/entry[@key='length']");
                var fileSize = lengthNode != null ? long.Parse(lengthNode.Value) : 0L;

                var fileInfo = new TorrentFileInfo(0, filepath, fileSize);
                fileList.Add(fileInfo);
            }
            else
            {
                // multi file
                var index = 0;
                foreach (var fileNode in fileNodeList)
                {
                    var filePathList = new List<string>();
                    var pathNodeList = fileNode.XPathSelectElements("./entry[@key='path']/list/*");
                    foreach (var path in pathNodeList)
                    {
                        filePathList.Add(path.Value);
                    }

                    var filepath = string.Join("/", filePathList);

                    var lengthNode = fileNode.XPathSelectElement("./entry[@key='length']");
                    var fileSize = lengthNode != null ? long.Parse(lengthNode.Value) : 0L;

                    var fileInfo = new TorrentFileInfo(index++, filepath, fileSize);
                    fileList.Add(fileInfo);
                }
            }

            model.TorrentFileInfoList = fileList;
            return model;
        }
    }
}