﻿// Copyright (c) cnaos 2017-2019
// All rights reserved.

namespace TorrentDump.model
{
    public enum FileIntegrityCheckState
    {
        UNKNOWN,
        OK,
        NG
    }
}