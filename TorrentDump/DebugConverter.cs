﻿// Copyright (c) cnaos 2016-2019
// All rights reserved.

using System;
using System.Globalization;
using System.Windows.Data;

namespace TorrentDump
{
    public class DebugConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //set a breakpoint here
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //set a breakpoint here
            return value;
        }
    }
}