﻿// Copyright (c) cnaos 2017-2019
// All rights reserved.

using System.IO;
using System.Linq;
using System.Windows;

namespace TorrentDump
{
    /// <summary>
    ///     MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_PreviewDragOver(object sender, DragEventArgs e)
        {
            e.Effects = e.Data.GetDataPresent(DataFormats.FileDrop, true) ? DragDropEffects.Copy : DragDropEffects.None;

            e.Handled = true;
        }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            var files = e.Data.GetData(DataFormats.FileDrop) as string[];
            var droppedTorrentFilePath = files.FirstOrDefault(file => file.EndsWith(".torrent"));
            if (droppedTorrentFilePath == null)
            {
                return;
            }

            var viewModel = DataContext as TorrentFileViewModel;
            viewModel.TorrentFilePath.Value = droppedTorrentFilePath;

            // Torrentファイルの解析
            if (viewModel.TorrentFileParseCommand.CanExecute())
            {
                viewModel.TorrentFileParseCommand.Execute();
            }
        }

        private void TextBoxTargetDirectory_Drop(object sender, DragEventArgs e)
        {
            var files = e.Data.GetData(DataFormats.FileDrop) as string[];
            var droppedFilePath = files[0];

            string strtParentName;
            if (File.Exists(droppedFilePath) && !Directory.Exists(droppedFilePath))
            {
                strtParentName = Path.GetDirectoryName(droppedFilePath);
            }
            else
            {
                strtParentName = droppedFilePath;
            }

            var viewModel = DataContext as TorrentFileViewModel;
            viewModel.TargetDirectory.Value = strtParentName;
        }
    }
}