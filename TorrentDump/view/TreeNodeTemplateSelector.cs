﻿// Copyright (c) cnaos 2016-2019
// All rights reserved.

using System.Windows;
using System.Windows.Controls;
using System.Xml;

namespace TorrentDump.view
{
    public class TreeNodeTemplateSelector : DataTemplateSelector
    {
        public DataTemplate ValueNodeDataTemplate { get; set; }
        public DataTemplate DictNodeDataTemplate { get; set; }
        public DataTemplate ListNodeDataTemplate { get; set; }
        public DataTemplate DefaultNodeDataTemplate { get; set; }

        public DataTemplate DictEntryNodeDataTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var element = container as FrameworkElement;
            if (element == null)
            {
                return null;
            }

            if (item == null)
            {
                return null;
            }

            if (item.GetType() == typeof(XmlElement))
            {
                var xmlElement = (XmlElement) item;

                switch (xmlElement.LocalName)
                {
                    case "integer":
                    case "str":
                    case "sha1":
                        return ValueNodeDataTemplate;
                    case "dict":
                        return DictNodeDataTemplate;
                    case "list":
                        return ListNodeDataTemplate;
                    case "entry":
                        return DictEntryNodeDataTemplate;
                }
            }

            return DefaultNodeDataTemplate;
        }
    }
}