﻿// Copyright (c) cnaos 2016-2019
// All rights reserved.

using System;
using System.Globalization;
using System.Windows.Data;
using ByteSizeLib;

namespace TorrentDump.view
{
    public class FileSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is long)) throw new ArgumentException("long型じゃないよ。", "value");
            var size = (long) value;
            var maxFileSize = ByteSize.FromBytes(size);

            return $"{maxFileSize.LargestWholeNumberValue:0.00} {maxFileSize.LargestWholeNumberSymbol}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}