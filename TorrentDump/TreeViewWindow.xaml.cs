﻿// Copyright (c) cnaos 2019-2019
// All rights reserved.

using System;
using System.Windows;
using System.Windows.Controls;

namespace TorrentDump
{
    /// <summary>
    ///     TreeViewWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class TreeViewWindow : Window
    {
        public TreeViewWindow()
        {
            InitializeComponent();
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void ButtonExpand_Click(object sender, RoutedEventArgs e)
        {
            var root = (TreeViewItem) TreeView.ItemContainerGenerator.ContainerFromIndex(0);
            root.IsExpanded = true;

            ExpandAll(root);
        }

        private void ExpandAll(ItemsControl items)
        {
            foreach (var obj in items.Items)
            {
                var childControl = items.ItemContainerGenerator.ContainerFromItem(obj) as ItemsControl;
                if (childControl == null)
                {
                    break;
                }

                ExpandAll(childControl);


                var item = childControl as TreeViewItem;
                item.IsExpanded = true;
            }
        }


        // ウィンドウを閉じたら、再び表示できるようにする。
        private void Window_Closed(object sender, EventArgs e)
        {
            var viewModel = DataContext as TorrentFileViewModel;
            viewModel.IsTreeViewWindowShowing.Value = false;
        }
    }
}