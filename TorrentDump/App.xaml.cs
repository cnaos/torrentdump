﻿// Copyright (c) cnaos 2017-2019
// All rights reserved.

using System.Windows;
using System.Windows.Threading;
using QuickConverter;

namespace TorrentDump
{
    /// <summary>
    ///     App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            // Setup Quick Converter.
            // Add the System namespace so we can use primitive types (i.e. int, etc.).
            EquationTokenizer.AddNamespace(typeof(object));
            // Add the System.Windows namespace so we can use Visibility.Collapsed, etc.
            EquationTokenizer.AddNamespace(typeof(Visibility));
        }

        public void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            var message =
                $"大変申し訳けありません。システムエラーが発生しました。\n type={e.Exception.GetType()}\nmessage={e.Exception.Message}\nsource={e.Exception.Source}\nstackTrace={e.Exception.StackTrace}";
            MessageBox.Show(message);
            e.Handled = true;
        }
    }
}