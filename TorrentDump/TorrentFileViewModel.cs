﻿// Copyright (c) cnaos 2017-2019
// All rights reserved.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Xml;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using TorrentDump.model;

namespace TorrentDump
{
    public class TorrentFileViewModel
    {
        // コア部分
        public TorrentFileModel Model;


        public TorrentFileViewModel()
        {
            // TorrentFilePathに文字列がはいってたら実行可能
            TorrentFileParseCommand = new[]
            {
                TorrentFilePath.Select(path => !string.IsNullOrWhiteSpace(path)),
                IsCheckExecuting.Select(exec => !exec)
            }.CombineLatestValuesAreAllTrue().ToReactiveCommand();

            TorrentFileParseCommand.Subscribe(_ =>
                {
                    // チェック結果などの破棄
                    ClearCheckResult();


                    Task.Run(() => { ParseTorrentFile(TorrentFilePath.Value); });
                }
            );

            // Torrentファイルのオープンコマンド 
            TorrentFileOpenCommand = new ReactiveCommand();
            TorrentFileOpenCommand.Subscribe(_ =>
            {
                var dialog = new OpenFileDialog();
                //パラメタ設定
                dialog.Title = "Torrentファイルを選んでください";
                dialog.Filter = "Torrent Files|*.torrent";
                //ダイアログ表示
                if (dialog.ShowDialog() == true)
                {
                    // 結果処理
                    TorrentFilePath.Value = dialog.FileName;
                }
                else
                {
                    return;
                }

                // Torrentファイルの解析
                if (TorrentFileParseCommand.CanExecute())
                {
                    TorrentFileParseCommand.Execute();
                }
            });

            // Torrentファイル情報のTreeView表示コマンド
            TreeViewCommand = new[]
            {
                IsTorrentFileParsed,
                IsTreeViewWindowShowing.Select(showing => !showing)
            }.CombineLatestValuesAreAllTrue().ToReactiveCommand();

            TreeViewCommand.Subscribe(_ =>
            {
                IsTreeViewWindowShowing.Value = true;
                var treeViewWindow = new TreeViewWindow();
                treeViewWindow.DataContext = this;
                treeViewWindow.Show();
            });

            // チェック対象ディレクトリの選択コマンド
            SelectTargetDirectoryCommand = new[]
            {
                IsCheckExecuting.Select(exec => !exec)
            }.CombineLatestValuesAreAllTrue().ToReactiveCommand();

            SelectTargetDirectoryCommand.Subscribe(_ =>
            {
                var dialog = new CommonOpenFileDialog();
                //パラメタ設定
                dialog.Title = "検証対象のディレクトリを選ぶ";
                dialog.IsFolderPicker = true;
                //ダイアログ表示
                if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    // 結果処理
                    TargetDirectory.Value = dialog.FileName;
                }
            });

            // チェックコマンド
            CheckCommand = new[]
            {
                IsTorrentFileParsed,
                TargetDirectory.Select(str => !string.IsNullOrEmpty(str)),
                IsCheckExecuting.Select(exec => !exec)
            }.CombineLatestValuesAreAllTrue().ToReactiveCommand();


            CheckCommand.Subscribe(_ =>
            {
                if (CancellationTokenSource.Value == null)
                {
                    CancellationTokenSource.Value = new CancellationTokenSource();
                }

                var token = CancellationTokenSource.Value.Token;

                Task.Factory.StartNew(() =>
                {
                    token.ThrowIfCancellationRequested();
                    CheckTorrentFiles(token);
                }, token).ContinueWith(t =>
                {
                    CancellationTokenSource.Value.Dispose();
                    CancellationTokenSource.Value = null;

                    if (t.IsCanceled)
                    {
                        // TODO:キャンセルされたときの処理
                    }
                });
            });

            // Checkキャンセルコマンド
            // TODO 実行可能状態の制御
            CheckCancelCommand = CancellationTokenSource.Select(s => s != null).ToReactiveCommand();
            CheckCancelCommand.Subscribe(_ => { CancellationTokenSource.Value.Cancel(); });


            IsCheckResultExists =
                PieceCheckResults.ObserveProperty(l => l.Count).Select(x => x > 0).ToReactiveProperty();

            // Resetコマンド(チェック実行中でない、チェック結果がある)
            ResetCommand = new[]
            {
                IsCheckExecuting.Select(executing => !executing),
                // TOOD PieceCheckResultsのサイズが0より大きいかのチェック
                PieceCheckResults.ObserveProperty(l => l.Count).Select(x => x > 0).ToReactiveProperty()
            }.CombineLatestValuesAreAllTrue().ToReactiveCommand();

            ResetCommand.Subscribe(_ =>
            {
                if (!IsCheckExecuting.Value)
                {
                    ClearCheckResult();
                }
            });
        }

        private CompositeDisposable Disposable { get; } = new CompositeDisposable();

        public XmlDataProvider XmlData { get; } = new XmlDataProvider();

        private ReactiveProperty<CancellationTokenSource> CancellationTokenSource { get; } =
            new ReactiveProperty<CancellationTokenSource>();

        // 情報表示関係
        public ReactiveProperty<string> TorrentFilePath { get; } =
            new ReactiveProperty<string>("torrentファイルをドロップしてください。");

        public ReactiveProperty<string> TargetDirectory { get; } = new ReactiveProperty<string>("");
        public ReactiveProperty<string> Announce { get; set; } = new ReactiveProperty<string>("announce init");
        public ReactiveProperty<string> CreatedBy { get; set; } = new ReactiveProperty<string>("created by init");
        public ReactiveProperty<DateTime> CreationDate { get; set; } = new ReactiveProperty<DateTime>();
        public ReactiveProperty<long> TotalSize { get; set; } = new ReactiveProperty<long>(0);

        public ReactiveCollection<TorrentFileInfo> TorrentFileInfos { get; } =
            new ReactiveCollection<TorrentFileInfo>();

        public ReactiveProperty<int> PieceLength { get; } = new ReactiveProperty<int>(0);
        public ReactiveProperty<int> CheckSuccessPieceCount { get; } = new ReactiveProperty<int>(0);
        public ReactiveCollection<string> PieceHashList { get; } = new ReactiveCollection<string>();
        public ReactiveProperty<bool> IsDownloadFilesIntegral { get; } = new ReactiveProperty<bool>(false);

        public ReactiveProperty<FileIntegrityCheckState> FileIntegrityCheckStatus { get; } =
            new ReactiveProperty<FileIntegrityCheckState>(FileIntegrityCheckState.UNKNOWN);

        /// <summary>
        ///     チェックの進捗
        /// </summary>
        public ReactiveProperty<double> CheckProgressPercent { get; } = new ReactiveProperty<double>(0.0);

        public ReactiveProperty<string> CheckProgressText { get; } = new ReactiveProperty<string>("");

        // コマンド制御用
        public ReactiveProperty<bool> IsTorrentFileParsed { get; } = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> IsCheckExecuting { get; } = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> IsCheckResultExists { get; }
        public ReactiveProperty<bool> IsTreeViewWindowShowing { get; } = new ReactiveProperty<bool>(false);

        // コマンド
        public ReactiveCommand TorrentFileOpenCommand { get; }
        public ReactiveCommand TorrentFileParseCommand { get; }
        public ReactiveCommand SelectTargetDirectoryCommand { get; }
        public ReactiveCommand CheckCommand { get; }
        public ReactiveCommand CheckCancelCommand { get; }
        public ReactiveCommand ResetCommand { get; }
        public ReactiveCommand TreeViewCommand { get; }

        // チェック結果
        public ReactiveCollection<PieceCheckResult> PieceCheckResults { get; } =
            new ReactiveCollection<PieceCheckResult>();

        // デバッグ用
        public ReactiveCollection<string> DebugLog { get; } = new ReactiveCollection<string>();

        /// <summary>
        ///     チェック結果のクリア
        /// </summary>
        private void ClearCheckResult()
        {
            PieceCheckResults.ClearOnScheduler();

            DebugLog.ClearOnScheduler();

            CheckProgressPercent.Value = 0;
            CheckProgressText.Value = "";

            CheckSuccessPieceCount.Value = 0;

            IsDownloadFilesIntegral.Value = true;
            FileIntegrityCheckStatus.Value = FileIntegrityCheckState.UNKNOWN;
        }

        /// <summary>
        ///     Torrentファイルを解析してModelを作る
        /// </summary>
        /// <param name="torrentFilePath"></param>
        /// <param name="viewModel"></param>
        private void ParseTorrentFile(string torrentFilePath)
        {
            var service = new TorrentDumpService();

            var model = service.ParseTorrentFile(torrentFilePath);
            Model = model;
            UpdateViewModel(Model);
        }

        /// <summary>
        ///     modelをviewに反映する
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="model"></param>
        private void UpdateViewModel(TorrentFileModel model)
        {
            // TODO あとでViewModelのXmlDataProviderをxdocに変える
            {
                var xmlDocument = new XmlDocument();
                using (var xmlReader = model.XDocument.CreateReader())
                {
                    xmlDocument.Load(xmlReader);
                }

                XmlData.Document = xmlDocument;
                XmlData.XPath = "*";
                XmlData.Refresh();
            }

            var rootElement = XmlData.Document.DocumentElement;
            var announceNode = rootElement.SelectSingleNode("/torrent/dict/entry[@key='announce']");
            Announce.Value = announceNode?.InnerText ?? "not found";

            var createdByNode = rootElement.SelectSingleNode("/torrent/dict/entry[@key='created by']");
            CreatedBy.Value = createdByNode?.InnerText ?? "not found";

            var createdDateNode = rootElement.SelectSingleNode("/torrent/dict/entry[@key='creation date']");
            if (createdDateNode != null)
            {
                var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var datetime = dtDateTime.AddSeconds(int.Parse(createdDateNode.InnerText));
                CreationDate.Value = datetime;
            }

            var pieceLengthNode =
                rootElement.SelectSingleNode("/torrent/dict/entry[@key='info']/dict/entry[@key='piece length']");
            PieceLength.Value = pieceLengthNode != null ? int.Parse(pieceLengthNode.InnerText) : 0;

            // pieceのハッシュ値の収集
            model.PieceHashList = new List<string>();
            PieceHashList.ClearOnScheduler();
            var pieceHashNodeList =
                rootElement.SelectNodes("/torrent/dict/entry[@key='info']/dict/entry[@key='pieces']/sha1");
            foreach (XmlNode hashNode in pieceHashNodeList)
            {
                var hash = hashNode.InnerText.Trim();
                model.PieceHashList.Add(hash);
                PieceHashList.AddOnScheduler(hash);
            }


            long totalSize = 0;
            TotalSize.Value = 0;
            TorrentFileInfos.ClearOnScheduler();
            IsTorrentFileParsed.Value = false;
            foreach (var fileInfo in model.TorrentFileInfoList)
            {
                totalSize += fileInfo.Size;
                TorrentFileInfos.AddOnScheduler(fileInfo);
            }

            TotalSize.Value = totalSize;
            IsTorrentFileParsed.Value = true;
        }


        /// <summary>
        ///     ダウンロードされたファイルをチェックする
        /// </summary>
        /// <param name="token"></param>
        public void CheckTorrentFiles(CancellationToken token)
        {
            IsCheckExecuting.Value = true;
            ClearCheckResult();

            updateCheckProgressText(0, PieceHashList.Count);

            var buffer = new byte[PieceLength.Value];
            DebugLog.AddOnScheduler($"buffer length={buffer.Length}");

            using (var stream = new BunchOfFilesStream(TargetDirectory.Value, Model.TorrentFileInfoList))
            {
                SHA1 crypto = new SHA1CryptoServiceProvider();
                foreach (var piece in Model.PieceHashList.Select((v, i) => new {Hash = v, Index = i}))
                {
                    Array.Clear(buffer, 0, buffer.Length);
                    var readLength = stream.Read(buffer, 0, buffer.Length);

                    var hashValue = crypto.ComputeHash(buffer, 0, readLength);
                    var hashString = BitConverter.ToString(hashValue).Replace("-", "");

                    var checkResult = new PieceCheckResult(piece.Index, piece.Hash);
                    checkResult.ActualFileHash = hashString;
                    checkResult.FilePath = stream.SelectedFileInfo.Path;
                    PieceCheckResults.AddOnScheduler(checkResult);

                    if (string.Compare(piece.Hash, hashString, StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        CheckSuccessPieceCount.Value += 1;

                        DebugLog.AddOnScheduler($"{piece.Index}/{Model.PieceHashList.Count}: OK");
                    }
                    else
                    {
                        IsDownloadFilesIntegral.Value = false;
                        FileIntegrityCheckStatus.Value = FileIntegrityCheckState.NG;
                        DebugLog.AddOnScheduler(
                            $"{piece.Index}/{Model.PieceHashList.Count}: NG {piece.Hash} / {hashString}");
                    }

                    if (token.IsCancellationRequested)
                    {
                        DebugLog.AddOnScheduler("---Cancelled---");
                        break;
                    }

                    updateCheckProgressText(piece.Index + 1, PieceHashList.Count);
                }
            }

            IsCheckExecuting.Value = false;
            // どこかでNGになってなかったら全部OKのはず
            if (FileIntegrityCheckStatus.Value == FileIntegrityCheckState.UNKNOWN)
            {
                FileIntegrityCheckStatus.Value = FileIntegrityCheckState.OK;
            }

            // TODO ファイルごとの一致/不一致を表示できるようにする
        }


        private void updateCheckProgressText(int i, int count)
        {
            var progress = (double) i / count * 100;
            CheckProgressPercent.Value = progress;
            CheckProgressText.Value = $"{progress:##0.0} ({i} / {count})";
        }
    }
}