TorrentDump

--------------

## 概要

windows用のtorrentファイルのダンプツールです。

ファイルの整合性チェックもできます。

## スクリーンショット

### メイン画面

![スクリーンショット](images/screenshot1.png)

### 詳細情報

![スクリーンショット](images/screenshot2.png)

